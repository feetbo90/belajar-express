/* file: mongodb.js */
 const MongoClient = require("mongodb").MongoClient
 // const connectionString = "mongodb://localhost:27017"; // tanpa
 const connectionString = "mongodb://user_latihan:123456@localhost:27017?authSource=admin"

 const client = new MongoClient(connectionString, {
    useUnifiedTopology: true
  });
 (async () => {
    try {
      await client.connect();
      // console.log("Server Database Connect")
    //   const db = client.db('latihan')
    //     // kode query ke collection quotes
    //   const quotes = await db.collection('products').find({ name: "Keyboard"}, { _id: false, name:true, stock:true, status: true}).toArray()
    //   console.log(quotes)
    } catch (error) {
  console.error(error); }
  })();

  module.exports = client

  