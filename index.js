/* file index.js */
const express = require('express')
const app = express()
const port = 3000
const path = require('path')
// ADD THIS
var cors = require('cors');


// app.use('/public',express.static('',path.join(__dirname, 'public')))

// const routers = require('./routers')
const routers = require('./routers_mongoose')
const bodyParser = require('body-parser') 
const log = (req, res, next) => {
    console.log(Date.now()+' '+req.ip+' '+req.originalUrl)
    next()
   }


// kode lain
// penggunaan middleware body parser
// parse x-www-form-url-encode

app.use(cors());
app.use(express.urlencoded({ extended: true }))
// parse JSON
app.use(express.json())
app.use(log)
app.use(routers)


// middleware menangani 404
// const notFound = (req, res, next) => {
//     res.json({
//     status: 'error',
//     message: 'resource tidak ditemukan',
//     })
//    }
//    app.use(notFound)

const errorHandling = (err, req, res, next) => {
    res.json({
    status: 'error',
    message: err.message,
    })
}
    app.use(errorHandling)

app.listen(port, () => console.log(`Server running at
        http://localhost:${port}`))