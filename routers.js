const express = require('express')
const multer = require('multer')
const upload = multer({ dest: 'public' })
const routers = express.Router()
// require dulu
const path = require('path')
const client = require('./connection')
// pencarian menggunakan id harus menggunakan ObjectId
const ObjectId = require('mongodb').ObjectId

// deklarasi routing lain
    routers.get('/products', async (req, res) => { if (client.isConnected()) {
    const db = client.db('latihan')
    // kode untuk menampilkan list products 
        const products =  await db.collection('products').find().toArray() 
        if (products.length>0){ res.send({
            status: 'success',
            message: 'list products ditemukan',
            data: products
        })
        } else {
            res.send({
            status: 'success',
            message: 'list products tidak ditemukan',
            }) 
        }
    } else {
        res.send({
            status: 'error',
            message: 'koneksi database gagal'
            })
    } })

    routers.get('/product/:id', async (req, res) => { if (client.isConnected()) {
        const db = client.db('latihan')
        const id = req.params.id
        const _id = (ObjectId.isValid(id))?ObjectId(id):id
        
        const product = await db.collection('products').findOne({
        _id: _id })
        
        res.send({
            status: 'success', 
            message: 'single product', 
            data: product
            })
        } else {
            res.send({
            status: 'error',
            message: 'koneksi database gagal'
            })
        }
    })

    routers.post('/product', multer().none(), async (req, res) => { if (client.isConnected()) {
        const { name, price, stock, status } = req.body 
        const db = client.db('latihan')
        const result = await db.collection('products').insertOne({ name: name,
                price: price,
                stock: stock,
                status: status
        })
        if (result.insertedCount == 1) {
        res.send({
        status: 'success',
        message: 'tambah product success',
        })
        } else {
        res.send({
        status: 'warning',
        message: 'tambah product gagal',
        }) }
        } else { res.send({
              status: 'error',
              message: 'koneksi database gagal'
            })
        } })

    routers.put('/product/:id', multer().none(), async (req, res) => { if (client.isConnected()) {
        const { name, price, stock, status } = req.body
        const db = client.db('latihan')
        const id = req.params.id
        const _id = (ObjectId.isValid(id))?ObjectId(id):id
        const result = await db.collection('products').updateOne(
        { _id: _id }, {
        $set: {
            name: name,
          price: price,
          stock: stock,
          status: status
        } }
        )
        if (result.matchedCount==1){
        res.send({
        status: 'success',
        message: 'update product success',
        })
        } else {
        res.send({
        status: 'warning',
        message: 'update product gagal',
        }) }
        } else { res.send({
            status: 'error',
            message: 'koneksi database gagal'
            })
        } })

        routers.delete('/product/:id', async (req, res) => { if (client.isConnected()) {
            const db = client.db('latihan')
            const id = req.params.id
            const _id = (ObjectId.isValid(id))?ObjectId(id):id
            const result = await db.collection('products').deleteOne(
            { _id: _id } )
            if (result.deletedCount==1){ res.send({
                    status: 'success',
                    message: 'delete product success',
                  })
            } else { res.send({
                    status: 'warning',
                    message: 'delete product gagal',
                  })
            }
            } else {
            res.send({
            status: 'error',
            message: 'koneksi database gagal'
            })
        } 
    })


    // upload dengan 1 file saja
    routers.post('/upload', upload.single('file'), (req, res) => {
        res.send(req.file) })
    routers.get('/', (req, res) => res.send('Hello World!')) 
    routers.post('/contoh', (req, res) => {
        res.send('request dengan method POST')
    })
    routers.put('/contoh', (req, res) => {
        res.send('request dengan method PUT')
    })
    routers.delete('/contoh', (req, res) => {
        res.send('request dengan method DELETE')
    })
    routers.all('/universal', function (req, res) {
        res.send('universal request dengan method '+req.method)
    })
    // request dengan menggunakan id
    routers.get('/post/:id', (req, res) => {
        res.send('artikel-'+req.params.id)
    })
    routers.get('/download', function (req, res) {
        const filename = 'logo.png'
        // res.sendFile(__dirname + '/' + filename)
        res.sendFile(path.join(__dirname, filename))   
    })
    routers.get('/foods', (req, res) => {
        // console.log(req.query)
        const json = {
                'status': 'success',
                'message': req.query.iqbal
             };
        res.write(JSON.stringify(json))
        res.end()
    })
    routers.post('/login', (req, res) => {
        const { username, password } = req.body
        res.send(`Anda login dengan username ${username} dan password
       ${password}`)
       })

module.exports = routers