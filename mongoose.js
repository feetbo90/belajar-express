/* file: mongoose */
const mongoose = require('mongoose') 
mongoose.connect('mongodb://user_latihan:123456@localhost:27017/latihan?authSource=admin', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

// membuat schema untuk collection quotes
// const quoteSchema = new mongoose.Schema({ word: String
// })

// // membuat model untuk schema quote
// const Belajar = mongoose.model('belajar', quoteSchema)

// const db = mongoose.connection
// db.on('error', console.error.bind(console, 'connection error:')) 
// db.once('open', () => {
//     // membuat quote baru
//     const belajar = new Belajar({ word: 'Besar pasak daripada tiang' })
    
//     // menyimpan belajar
//     belajar.save((error, belajar) => {
//     if (error) return console.error(error)
//     console.log(belajar)
//     })
// })
const mongoose = require('mongoose') 
  mongoose.connect('mongodb://user_latihan:123456@localhost:27017/latihan?authSource=admin', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
// membuat schema
const productSchema = new mongoose.Schema({ name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50
}, price: {
    type: Number,
    required: true,
    min: 1000,
    max: 1000000,
  },
  stock: Number,
  status: { type: Boolean, default: true },
})
// membuat model
const Product = mongoose.model('Product', productSchema);
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:')) 
db.once('open', async () => {
  // query di sini
  const query = Product.find()
  query.where({ 'stock': { $gte : 5} })
  const list_products = await query.exec()
  console.log(list_products)
})